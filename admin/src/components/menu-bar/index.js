import React from 'react';
import { useQuery } from 'react-query';

import {
  PIPELINE_STATUS,
  storage,
  TRIGGER_TOKEN,
  routes,
  request,
} from '../../constants';

export const MenuBar = ({ lastPipeline, setLastPipeline }) => {
  const [selected, setSelected] = React.useState('dev');

  const { data: branches = [], error, isFetching } = useQuery(
    'branches',
    async () => {
      const { data } = await request.get(routes.branches.get);
      console.log(data)
      return data;
    }
  );

  const onChange = (e) => {
    setSelected(e.target.value);
  };

  const runPipeline = async () => {
    if (!selected) {
      return alert('Select a branch before run pipeline.');
    }

    try {
      const { data } = await request.post(
        routes.pipelines.trigger(selected),
        { token: TRIGGER_TOKEN },
      );

      storage.set(data.id);
      setLastPipeline(data);
    }
    catch (error) {
      alert(`Message: ${error.message} \n Error: ${error.response.data.error}`);
      console.error(error, error.response);
    }
  };

  if (isFetching) {
    return 'Loading...';
  }
  if (error) {
    alert(error.message);
    console.error(error);
    return JSON.stringify(error, null, 2);
  }

  const inProgress =
    lastPipeline.status === PIPELINE_STATUS.PENDING ||
    lastPipeline.status === PIPELINE_STATUS.PREPARING ||
    lastPipeline.status === PIPELINE_STATUS.RUNNING;

  return (
    <div className="menu">
      <label className="pipeline-select">
        Branch
        <select value={selected} onChange={onChange} disabled={isFetching}>
          {branches.map((item, k) => (
            <option key={k} value={item.name}>
              {item.name}
            </option>
          ))}
        </select>
      </label>
      <button
        type="button"
        disabled={inProgress || !selected || isFetching}
        onClick={runPipeline}
        className="run-pipeline-button"
      >
        Run pipeline
      </button>
    </div>
  );
};

import React from 'react';
import moment from 'moment';
import { isEqual } from 'lodash';
import { useQuery } from 'react-query';

import {
  request,
  routes,
  PIPELINE_STATUS,
} from '../../constants';

const getIcon = (url) => `https://gitlab.com${url}`;

const TableRow = ({ pipeline, cancelPipeline, isCanceling }) => {
  const inProgress =
    pipeline.status === PIPELINE_STATUS.PENDING ||
    pipeline.status === PIPELINE_STATUS.PREPARING ||
    pipeline.status === PIPELINE_STATUS.RUNNING;
  const showCancelBtn =
    pipeline.status !== PIPELINE_STATUS.CANCELED &&
    pipeline.status !== PIPELINE_STATUS.FAILED &&
    pipeline.status !== PIPELINE_STATUS.SUCCESS;

  const linkClassNames = `pipeline-link ${
    pipeline.status === PIPELINE_STATUS.FAILED
      ? 'failed'
      : pipeline.status === PIPELINE_STATUS.SUCCESS
        ? 'success'
        : inProgress
          ? 'running'
          : ''
  }`;

  return (
    <tr>
      <td>
        <a
          className={linkClassNames}
          href={pipeline.web_url}
          target="_blank"
          title="Open pipeline on github"
        >
          {pipeline.detailed_status?.favicon && (
            <img
              className="pipeline-link__icon"
              src={getIcon(pipeline.detailed_status?.favicon)}
              alt="status"
            />
          )}
          {inProgress ? PIPELINE_STATUS.RUNNING : pipeline.status}
        </a>
      </td>
      <td>
        <div className="pipeline-time-info__group">
          {inProgress && 'In progress'}
          {!inProgress && pipeline.duration && (
            <p className="pipeline-time-info">
              Duration:&nbsp;&nbsp;
              <time>
                {moment.utc(pipeline.duration * 1000).format('HH:mm:ss')}
              </time>
            </p>
          )}
          {!inProgress && pipeline.finished_at && (
            <p className="pipeline-time-info">
              Finished:&nbsp;&nbsp;
              <time>{moment(pipeline.finished_at).fromNow()}</time>
            </p>
          )}
        </div>
      </td>
      <td className="branch-col">{pipeline.ref}</td>
      <td className="actions-col">
        {showCancelBtn && (
          <button
            type="button"
            id={pipeline.id}
            onClick={cancelPipeline}
            disabled={isCanceling}
            className="cancel-button"
          >
            Cancel
          </button>
        )}
      </td>
    </tr>
  );
};

const loadLastPipeline = async (savedPipelineId = storage.get()) => {
  let lastPipeline;

  if (savedPipelineId) {
    const { data } = await request.get(routes.pipelines.get(savedPipelineId));
    lastPipeline = data;
  } else {
    const allPipelines = await request.get(routes.pipelines.get_all);
    const { data } = await request.get(routes.pipelines.get(allPipelines.data[0].id));
    lastPipeline = data;
  }

  return lastPipeline;
};

const useLoadLastPipeline = (lastPipelineId) => {
  const { data, error, isFetching, refetch } = useQuery(
    ['last-pipeline', lastPipelineId],
    ({ queryKey }) => loadLastPipeline(queryKey[1]),
    { retry: false }
  );

  return {
    refetch,
    newLastPipeline: data,
    error,
    isFetching
  };
};

const useCancelPipeline = (pipelineId, enabled) => {
  const retry = React.useRef(10);

  const { data, isFetching } = useQuery(
    'cancel-pipeline',
    () => loadLastPipeline(pipelineId),
    { refetchInterval: 5000, enabled }
  );

  if (data?.status === PIPELINE_STATUS.CANCELED) {
    retry.current = false;
  }

  return {
    canceledPipeline: data,
    isCanceling: isFetching
  };
};

export const PipelineBar = ({ lastPipeline, setLastPipeline }) => {
  const { newLastPipeline, error, isFetching } = useLoadLastPipeline(
    lastPipeline?.id || null
  );

  const [enabledCanceling, setEnabledCanceling] = React.useState(false);
  const { canceledPipeline, isCanceling } = useCancelPipeline(lastPipeline.id, enabledCanceling);

  React.useEffect(() => {
    if (newLastPipeline?.id && !isEqual(newLastPipeline, lastPipeline)) {
      setLastPipeline(newLastPipeline);
    }

    if (canceledPipeline) {
      setLastPipeline(canceledPipeline);
    }
  }, [newLastPipeline, canceledPipeline]);

  const cancelPipeline = async () => {
    if (window.confirm(`Stop pipeline #${lastPipeline.id}?`)) {
      await request.post(routes.pipelines.cancel(lastPipeline.id));
      setEnabledCanceling(true);
    }
  };

  if (error) {
    console.error(error);
    return JSON.stringify(error, null, 2);
  }

  return (
    <table>
      <thead>
        <tr>
          <th>Status</th>
          <th>Info</th>
          <th>Branch</th>
          <th />
        </tr>
      </thead>
      <tbody>
      {isFetching ? (
        <tr><td className="loading-row" colSpan="4">Loading...</td></tr>
      ) : (lastPipeline && (
        <TableRow
          pipeline={lastPipeline}
          cancelPipeline={cancelPipeline}
          isCanceling={isCanceling}
        />
      ))}
      </tbody>
    </table>
  );
};

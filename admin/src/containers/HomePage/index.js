/*
 *
 * HomePage
 *
 */

import React from 'react';

import { MenuBar } from '../../components/menu-bar';
import { PipelineBar } from '../../components/pipeline-bar';

const HomePage = () => {
  const [lastPipeline, setLastPipeline] = React.useState({});

  return (
    <div className="deploy-plugin">
      <div style={{ display: 'flex', gap: '20px', marginTop: '20px' }}>
        <a href="https://gitlab.com/vsamofal/softkit-gatsby-site/-/commits/master">master: <img alt="pipeline status" src="https://gitlab.com/vsamofal/softkit-gatsby-site/badges/master/pipeline.svg" /></a>
        <a href="https://gitlab.com/vsamofal/softkit-gatsby-site/-/commits/dev">dev: <img alt="pipeline status" src="https://gitlab.com/vsamofal/softkit-gatsby-site/badges/dev/pipeline.svg" /></a>
      </div>
      <MenuBar lastPipeline={lastPipeline} setLastPipeline={setLastPipeline} />
      <PipelineBar lastPipeline={lastPipeline} setLastPipeline={setLastPipeline} />
    </div>
  );
};

export default React.memo(HomePage);

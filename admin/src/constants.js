import axios from 'axios';

export const routes = {
  branches: {
    get: '/repository/branches',
  },
  pipelines: {
    trigger: (ref) => `/ref/${ref}/trigger/pipeline`,
    get_all: '/pipelines.json',
    get: (pipeline_id) => `/pipelines/${pipeline_id}`,
    cancel: (pipeline_id) => `/pipelines/${pipeline_id}/cancel`,
  },
};

// https://gitlab.com/api/v4/projects/32397836/ref/REF_NAME/trigger/pipeline?token=TOKEN
// https://gitlab.com/api/v4/projects/32397836/ref/dev/trigger/pipeline?token=d7dce2eb32d8811ecebf887dba1416


export const PIPELINE_STATUS = {
  CREATED: 'created',
  PREPARING: 'preparing',
  PENDING: 'pending',
  RUNNING: 'running',
  SUCCESS: 'success',
  FAILED: 'failed',
  CANCELED: 'canceled',
  SKIPPED: 'skipped',
  MANUAL: 'manual',
  SCHEDULED: 'scheduled',
};

export const storage = {
  set: (pipeline_id) => sessionStorage.setItem('pipeline_id', pipeline_id),
  get: () => sessionStorage.getItem('pipeline_id'),
  remove: () => sessionStorage.removeItem('pipeline_id'),
};

export const PROJECT_ID = '32397836';
export const GITLAB_BASE_URL = `https://gitlab.com/api/v4/projects/${PROJECT_ID}`;
export const ACCESS_TOKEN = 'glpat-iEuRuqxT6kpewqZu5Shv';
export const TRIGGER_TOKEN = 'd7dce2eb32d8811ecebf887dba1416';

export const request = {
  get: async (route) => axios.get(
		GITLAB_BASE_URL + route,
		{
			headers: {
				'PRIVATE-TOKEN': ACCESS_TOKEN,
			}
		},
	),
  post: async (route, params) => axios.post(
		GITLAB_BASE_URL + route,
		null,
		{
			headers: {
				'PRIVATE-TOKEN': ACCESS_TOKEN,
			},
			params
  	},
	),
};
